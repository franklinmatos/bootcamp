export const swaggerDocument = 
{
  "swagger": "2.0",
  "info": {
    "description": "Chalenge Module 02 - Grades",
    "version": "0.0.1",
    "title": "Swagger Chalenge Module 02 - Grades",
    "contact": {
      "email": "franklinmatos@gmail.com"
    },
    },
    "host": "http://localhost:8081/",
    "tags":
    [
       {  "name": "grades",
          "description": "List of grades for students control",
          "externalDocs": {
            "description": "Find out more",
            "url": "http://localhost:8081"
          }
      }
    ],
    "paths": {
      "/grades/create": {
        "post": {
          "tags": [
            "grades"
          ],
          "summary": "Add a new item in a grade list",
          "description": "",
          "operationId": "create",
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "- in": "body",
              "name": "student",
              "type": "String",
              "description": "",
              "required": true
            },
            {  
              "name": "subject",
              "type": "String",
              "description": "",
              "required": true
            },
            {
              "name": "type",
              "type": "String",
              "description": "",
              "required": true
            },
            {
              "name": "value",
              "type": "String",
              "description": "",
              "required": true
            }
          ],
          "responses": {
            "200": {
              "description": "Perfect Execution"
            },
            "400": {
              "description": "Problem executed."
            }
          },
        },
      },
    },
   }
  