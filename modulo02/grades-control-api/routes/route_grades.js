import express from "express";
import  { promises as fs}  from "fs";
import Grade from  "../model/grade.js";

const { readFile, writeFile} = fs;

const router = express.Router();

// questao 01
    router.post("/create", async  ( req,res) => {
    
     try{
        const lista = JSON.parse( await readFile("grades.json") );
        let nextId = lista.nextId;

        let grade = new Grade();
        grade = grade.mapear(req.body);
        grade.id = nextId;
        
        lista.grades.push(grade);
        lista.nextId = nextId+1;
       
        await writeFile("grades.json", JSON.stringify(lista));
       res.end(); 
    }catch(err){
        res.status(400).send({error: err.message});
    }  
   });
    
   //questao 02
   router.patch("/update", async (req,res) => {
    try{
        const lista = JSON.parse( await readFile("grades.json") );
        
        let grade = new Grade();
        grade = grade.mapear(req.body);
        lista.grades.forEach(element => {
            if(element.id === grade.id){
                
                element.student = !grade.student ? element.student : grade.student;
                element.subject = !grade.subject ? element.subject : grade.subject;
                element.type = !grade.type ?  element.type: grade.type;
                element.value = !grade.value ? element.value: grade.value;
                
            }
            
        });
        await writeFile("grades.json", JSON.stringify(lista));
        res.send(grade);
    }catch(err){
        res.status(400).send({error: err.message});
    }  
   });

   // questao 03
   router.delete("/delete",async (req,res) => {
       try{
            const lista = JSON.parse( await readFile("grades.json") );
            let grade = new Grade();
            let count = 0;
            grade = grade.mapear(req.body);
            lista.grades.forEach(element => {
                if(element.id === grade.id){
                    lista.splice(count,1);
                }
                count ++;
            });
            await writeFile("grades.json", JSON.stringify(lista));
            res.send(grade);
            
        }catch(err){
            res.status(400).send({error: err.message});
        }  
    });

    // questao 04
    router.get("/find",async (req,res) => {
        try{
             const lista = JSON.parse( await readFile("grades.json") );
             let grade = new Grade();
             let count = 0;
             grade = grade.mapear(req.body);
             lista.grades.forEach(element => {
                 if(element.id === grade.id){
                     grade = element;
                 }
             });
             if(!grade.student){
                return res.send({ err: 404,  message: "Nenhum elemento Encontrado"}); 
             }else{
                res.send(grade);
             }
             
             
         }catch(err){
             res.status(400).send({error: err.message});
         }  
     });

     //questao 05
     router.get("/findValue",async (req,res) => {
        try{
             const lista = JSON.parse( await readFile("grades.json") );
             let grade = new Grade();
             let sum = 0;
             grade = grade.mapear(req.body);
             lista.grades.forEach(element => {
                 if(element.student === grade.student && element.subject === grade.subject){
                     sum += element.value;
                     
                 }
             });
             
                return res.send({ aluno: grade.student,  value: sum}); 
             
         }catch(err){
             res.status(400).send({error: err.message});
         }  
     });

      //questao 06
      router.get("/avg",async (req,res) => {
        try{
             const lista = JSON.parse( await readFile("grades.json") );
             let grade = new Grade();
             let sum = 0;
             let avg = 0;
             let count = 0;
             grade = grade.mapear(req.body);
             lista.grades.forEach(element => {
                 if(element.subject === grade.subject && element.type === grade.type){
                     sum += element.value;
                     count++;
                 }
             });
             avg = sum / count;
             return res.send({ 
                 subject: grade.subject, type:grade.type ,sum: sum,count: count, avg: avg}); 
         }catch(err){
             res.status(400).send({error: err.message});
         }  
     });
   

export default router;