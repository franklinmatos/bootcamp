 class Grade {
    //propriedades e funções da classe aqui

    // var id ;
    // var student;
    // var subject;
    // var type;
    // var value;
    // var timestam;
    
    constructor(id = 0, student = '', subject = '',type = '',value = 0,timestamp = new Date() ){
        this.id = id;
        this.student = student;
        this.subject = subject;
        this.type = type;
        this.value = value;
        this.timestamp = timestamp;
    };
    /**
     * mapeia os parametro passados de body e devolve o objeto preechido.
     * @param {*} body 
     */
    mapear(body){
        
         let retorno = {
            id : body.id || 0,
            student : body.student,
            subject : body.subject,
            type : body.type,
            value : body.value,
            timestamp : ''
         }
         return retorno;
    };
  
} 

export default Grade;