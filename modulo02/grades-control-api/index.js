import  { promises as fs}  from "fs";
import express from "express";
import gradesRouter from "./routes/route_grades.js";
import { swaggerDocument } from "./doc.js";
import swaggerUI from "swagger-ui-express";

const { readFile, writeFile} = fs;
const app = express();
const port = 8081;
app.use(express.json());
app.use("/grades", gradesRouter);
app.use("/doc", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

//codigo executado sempre não importando quem passe por aqui
app.use( (req, res, next ) => {
    console.log('app.use');
   next();
});

app.listen(port, async () => {
    try{
        await readFile('grades.json');
        console.log("API Started!  Porta:"+port);
    }catch(err){
        const inicialJson = {
            nextId: 1 ,
            grades: []
        };
       await  writeFile('grades.json', JSON.stringify(inicialJson)).then(
            () =>{
                console.log("API Started and created file!");
            }
        ).catch(err => {
            console.log(err);
        });
    }
});