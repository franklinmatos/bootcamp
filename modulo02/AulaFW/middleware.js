import express from "express";

const app = express();
app.use(express.json());

//codigo executado sempre não importando quem passe por aqui
app.use( (_req, _res, next) => {
    console.log(new Date());
    next();
});

app.get("/teste", (_req,res) =>{
    console.log("get");
    res.end();
    console.log("end");
});


app.listen(4003, () => {
    console.log("Api Iniciada!!");
})