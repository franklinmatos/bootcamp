import {promises as fs} from "fs";

init();
//writeReadJson();

async function init(){
    try{

        await fs.writeFile("teste/teste.txt","teste escrita em arquivo");
        await fs.appendFile("teste/teste.txt","\n\nteste appendfile no arquivo gerado anteriormente");
        const data = await fs.readFile("teste/teste.txt","utf-8");
        console.log( typeof (data));

        
    }catch(err){
        console.log(err);
    }
}


async function writeReadJson(){
    try{
        const arrayCarros = ["Gol", "Palio","Onix"];
        const obj = {
            carros : arrayCarros
        }
        await fs.writeFile("teste/teste.json",JSON.stringify(obj));

        const dados = await fs.readFile("teste/teste.json");
        console.log(JSON.parse(dados));
    }catch(err){
        console.log(err);
    }

}
