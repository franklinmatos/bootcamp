import  fs from "fs";
import readline from "readline";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function init(){
    try{

       const estados  = JSON.parse ( await fs.readFile("dataBase/Estados.json") );
       let cidades = JSON.parse ( await fs.readFile("dataBase/Cidades.json") );
         
       await estados.forEach(estado => {
            const cid = cidades.filter( (cidade) =>{
               return estado.ID === cidade.Estado
            } );
            fs.writeFile(`dataBase/${estado.Sigla}.json`, JSON.stringify ( cid ) );
        });
         
    }catch(err){
        console.log(err);
    }

}

//init();
//questao02();
questao0304();

//questao0506();

questao0506("ASC"); //ASC para crescente | DESC para decrescente

localizaCidadesGeral("desc"); //ASC para crescente | DESC para decrescente
function questao02(){
    try{
        let entrada = 'AC';
        rl.question("Informe a UF do Estado que deseja ler: ", uf => {
            console.log(uf);
            entrada = uf;
            let cidades =  fs.readFile(`dataBase/${entrada}.json`)
            .then(result => {
                let dados = JSON.parse(result);
                console.log(dados.length);
                rl.close();
            })
            .catch(err => {
                console.log(err);
                rl.close();
            });
        });
 
    }catch(err){
        console.log(err);
    }
}

async function questao0304(){
    try{
        let quantidades = new Array();;
        const estados  = JSON.parse ( fs.readFileSync("dataBase/Estados.json") );
        await estados.forEach(estado => {
             let cidades = JSON.parse( fs.readFileSync(`dataBase/${estado.Sigla}.json`) );
             let obj = {
                 estado: estado.Sigla,
                 qtd_cidades: cidades.length
             }
             quantidades.push(obj);
         });
         //array ordenado em ordem crescente
          quantidades.sort( function (a,b) {
              return a.qtd_cidades > b.qtd_cidades ? 1 : a.qtd_cidades < b.qtd_cidades ? -1 : 0;
          });
          //console.log(quantidades);
          //criar array com os 5 primeiros
          let menores = new Array();
          for(let i = 0; i < 5; i++){
              menores.push(quantidades[i]);
          }
          console.log('QUESTAO 04');
          console.log(menores);
          //criar array com os 5 ultimos
          let maiores = new Array();
          let total = quantidades.length
          let j = 0;
          while( j < 5 ){
            maiores.push(quantidades[total - 1]);
            total--;
            j++;
          }
          console.log('QUESTAO 03');
          console.log(maiores);

        //   const ln = quantidades.reduce((r,s) =>{
        //       return r.nome.length > s.nome.length ? r :  r.nome.length < s.nome.length ? s : 0 ;
        //   });
        //   console.log(" //==========================\\");
        //   console.log(ln);
        //   console.log(" //==========================\\");
     }catch(err){
         console.log(err);
     }
}

async function questao0506(ordem){
    try{

        const estados  = JSON.parse ( fs.readFileSync("dataBase/Estados.json") );

        let auxArray = new Array();
         estados.forEach(estado => {

            auxArray.push(  localizaCidades(estado, ordem) );
        });

    }
    catch(err){
        console.log(err);
    }
}

function ordenarArrayCrescente(lista){
    lista.sort( function (a,b) {
        return a.tamanho > b.tamanho ? 1 : a.tamanho < b.tamanho ? -1 : 0;
    });
    return lista;
}

function ordenarArrayDeCrescente(lista){
    lista.sort( function (a,b) {
        return a.tamanho < b.tamanho ? 1 : a.tamanho > b.tamanho ? -1 : 0;
    });
    return lista;
}

async function localizaCidades(estado,ordem){
    let resultado = {};
    let aux = [];
    let cidades = JSON.parse( fs.readFileSync(`dataBase/${estado.Sigla}.json`) );
    
    let auxCidades = mapearCidades(cidades);
    
    if(ordem === 'ASC'){
         aux = ordenarArrayCrescente(auxCidades);
    }else{
         aux = ordenarArrayDeCrescente(auxCidades);
    }
    
   
    if(parseInt(estado.ID) === parseInt(aux[0].estado)){
     resultado = {
        nome: aux[0].nome,
        estado: estado.Sigla  ,
        tamanho: aux[0].tamanho
     };
     
    }
    console.log(`${resultado.estado} - ${resultado.nome} - ${resultado.tamanho} `);
    return resultado;
}


async  function localizaCidadesGeral(ordem){
    let cidades = JSON.parse( fs.readFileSync(`dataBase/Cidades.json`) );
    let estados = JSON.parse( fs.readFileSync(`dataBase/Estados.json`) );
    let aux = [];
    let auxCidades = mapearCidades(cidades);
   
    if(ordem === 'ASC'){
        aux = ordenarArrayCrescente(auxCidades);
   }else{
        aux = ordenarArrayDeCrescente(auxCidades);
   }
  
   let cidade = {
        nome: aux[0].nome,
        estado: aux[0].estado ,
        tamanho: aux[0].tamanho,
        uf: ''
    }
  
     let resultado = estados.filter( (elem) =>{
         
        if( parseInt(elem.ID) === parseInt(cidade.estado)){
           
            return elem.Sigla;
        }
     });
     cidade.uf = resultado[0].Sigla;

     console.log(`${cidade.uf} - ${cidade.nome} - ${cidade.estado} `);
}

function mapearCidades(cidades){
    let auxCidades = [];
    cidades.forEach(cid => {
        let obj = {
            nome: cid.Nome ,
            tamanho: cid.Nome.length,
            estado: cid.Estado ,
        }
        auxCidades.push(obj);
    });
    return auxCidades;
}