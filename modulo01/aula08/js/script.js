console.log('Aula 03 - cap 03');

var input1 = document.querySelector('#input1');
input1.value = 'Franklin Correia';

window.addEventListener('load',start);

function start(){
    console.log('Aula 04 - cap 03');
    console.log('Página totalmente carregada');

   
    var nameInput = document.querySelector('#input1');
    nameInput.addEventListener('keyup',countName);
    //span.textContent="Contador de Caracteres.";

    var form = document.querySelector('form');
    form.addEventListener('submit',preventSubmit);
}

function countName(event){
    
    var count = event.target.value;
    
    var span = document.querySelector('#nameLength');
    span.textContent = count.length;    
}

function preventSubmit(event){
    event.preventDefault();
    console.log('preventSubmit - preventDefault');
}