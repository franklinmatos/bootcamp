window.addEventListener('load',start);

var globalNames = ['João','Maria','José'];
var inputName = null;

function start(){
    console.log('start');
    preventFormSubmit();
    inputName = document.querySelector('#inputName');
    activateInput();
}

function preventFormSubmit(){
    function handleFormSubmit(event){
        event.preventDefault();   
    }
    
    var form = document.querySelector('form');
    form.addEventListener('submit', handleFormSubmit);

    
}

function activateInput(){
   
    function insertName(newName){
       globalNames.push(newName);
       render();
   }
   
   function handleTyping(event){
       if ( event.key === 'Enter') {
        insertName(event.target.value);
        }
    }

    inputName.focus();
    inputName.addEventListener('keyup', handleTyping);

    function render(){
        function createDeleteButton(index){
            
            function deleteClickButton(){
                globalNames.splice(index,1);
                render();
            }

            var button = document.createElement('button');
            button.textContent = 'X';
            button.classList.add('deleteButton');
            button.addEventListener('click',deleteClickButton);
            return button;
        }

        var divNames = document.querySelector('#names');
        divNames.innerHTML = '';
       
        var ul = document.createElement('ul');
       
        for(var i  = 0; i < globalNames.length; i++){
            var currentName = globalNames[i]; 
            var button = createDeleteButton(i);
            var li = document.createElement('li');
            
            var span = document.createElement('span');
            span.classList.add('clickable');
            span.textContent = currentName;
            
            li.appendChild(button)
            li.appendChild(span)
            ul.appendChild(li);
        }
        
        divNames.appendChild(ul);
        clearInput();
    }

    function clearInput(){
        inputName.value = "";
        inputName.focus();
    }
}

